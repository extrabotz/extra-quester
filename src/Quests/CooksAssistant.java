package Quests;

import org.osbot.rs07.api.Chatbox;
import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.model.*;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.utility.ConditionalSleep;

import java.util.List;

public class CooksAssistant extends QuestObject {
    private Area cooksKitchen;
    private Area wheatFarm;
    private Area wheatTower;
    private Area wheatTowerBottom;
    private Area chickenFarm;
    private Area bucketLoc;
    private Area dairyFarm;
    private Area bank;

    private Area[] area = {
            new Area(3206, 3216, 3211, 3212), // cook's kitchen (pot)
            new Area(3162, 3293, 3160, 3295), // wheat farm (wheat)
            new Area(3165, 3307, 3167, 3306).setPlane(2), // wheat tower (flower)
            new Area(3164, 3305, 3166, 3307), // wheat tower bottom
            new Area(3179, 3298, 3184, 3294), // chicken farm (egg)
            new Area(3225, 3293, 3228, 3291), // bucket (bucket)
            new Area(3253, 3277, 3257, 3273), // dairy farm (milk)
            new Area(3210, 3219, 3207, 3217).setPlane(2) // Lumbridge Bank
    };

    private String[] items = new String[]{"Egg", "Bucket", "Wheat", "Pot of flour", "Bucket of milk", "Pot"};

    public CooksAssistant() {
        cooksKitchen = area[0];
        wheatFarm = area[1];
        wheatTower = area[2];
        wheatTowerBottom = area[3];
        chickenFarm = area[4];
        bucketLoc = area[5];
        dairyFarm = area[6];
        bank = area[7];
    }

    @Override
    public int run() throws InterruptedException {
        if(!checkInventory()) {
            return 1;
        }
        switch (configs.get(29)) {
            case 0:
                startQuest();
                break;
            case 1:
                if (questComplete()) {
                    log("Quest Complete!");
                    return 2;
                }
                collectFlour();
                collectEgg();
                collectBucket();
                collectMilk();
                break;
            case 2:
                log("Quest has already been completed!");
            default:
                return 2;
        }
        return 1;
    }

    private boolean checkInventory() throws InterruptedException {
        boolean inventoryFull = getInventory().isFull() || getInventory().getEmptySlots() < 4;
        if(inventoryFull && !bank.contains(myPosition())) {
            walkToArea(bank, "Lumbridge Bank");
            return false;
        } else if (inventoryFull && bank.contains(myPosition())) {
            openBank();
            bankItems();
            takeBankItems();
            return true;
        } else {
            log("Inventory does not need to be banked!");
            return true;
        }
    }

    private void openBank() throws InterruptedException {
        if(!getBank().isOpen()) {
            getBank().open();
            log("Opening Bank...");
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return getBank().isOpen();
                }
            }.sleep();
        } else {
            log("Bank is already open!");
        }
    }

    private void takeBankItems() {
        for(String item: items) {
            if(getBank().contains(item)) {
                log("Found item for quest: " + item);
                getBank().withdraw(item, 1);
            }
        }
    }

    private void bankItems() {
        if(getBank().depositAll()) {
            log("Deposited all items!");
        } else {
            log("There was an issue depositing all items!");
        }
    }

    private void walkToArea(Area area, String location) {
        log("Walking to: " + location);
        if (!area.contains(myPosition())) {
            getWalking().webWalk(area);
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return area.contains(myPosition());
                }
            }.sleep();
        } else {
            log("Already in Area: " + location);
        }
    }

    private void startQuest() throws InterruptedException {
        // walk to the kitchen
        walkToArea(cooksKitchen, "Kitchen");
        // talk to the cook and start the quest
        talkToCook();
    }

    private void takeItem(String name, Area area) {
        if (!getInventory().contains(name)) {
            // if the inventory doesn't contain the item by name
            walkToArea(area, name);
            // find the item on the ground
            Entity item = getGroundItems().closest(name);
            // take the item
            if (item == null) {
                // sleep until the item exists
                new ConditionalSleep(10000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return item.isVisible();
                    }
                }.sleep();
            } else if (item.interact("Take")) {
                // sleep until the inventory contains the item or the item exists
                new ConditionalSleep(5000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getInventory().contains(name);
                    }
                }.sleep();
            }
        } else {
            log("Inventory Already Contains: " + name);
        }
    }


    private void pickWheat() {
        if (!getInventory().contains("Grain") &&
                !getChatbox().contains(Chatbox.MessageType.ALL, "You put the grain in the hopper.") &&
                !wheatTower.contains(myPosition()) && !wheatTowerBottom.contains(myPosition())) {
            // if the inventory doesn't contain the item by name
            getTabs().open(Tab.INVENTORY);
            walkToArea(wheatFarm, "Wheat Farm");
            log("In the Wheat Area");
            // find the item on the ground
            if (wheatFarm.contains(myPosition())) {
                RS2Object item = getObjects().closest(wheatFarm, "Wheat");
                // take the item
                if (item != null) {
                    if (item.interact("Pick")) {
                        // sleep until the inventory contains a the item picked
                        new ConditionalSleep(5000) {
                            @Override
                            public boolean condition() throws InterruptedException {
                                return getInventory().contains("Grain");
                            }
                        }.sleep();
                        log("Wheat picked!");
                    }
                } else {
                    log("Wheat not found!");
                }
            }
        }
    }

    private void hopperInteraction() throws InterruptedException {
        if (!getChatbox().contains(Chatbox.MessageType.ALL, "You operate the hopper. The grain slides down the chute.")) {
            if (!wheatTower.contains(myPosition())) {
                walkToArea(wheatTower, "Wheat Tower");
            }
            if (!getChatbox().contains(Chatbox.MessageType.ALL, "There is already grain in the hopper.")) {
                getTabs().open(Tab.INVENTORY);
                getInventory().interact("Use", "Grain");
            } else {
                getInventory().deselectItem();
            }
            if (wheatTower.contains(myPosition())) {
                List<RS2Object> hopper = objects.filter(o -> o.getName().equals("Hopper"));
                if (hopper != null && getInventory().contains("Grain")) {
                    hopper.get(0).interact("Use");
                    new ConditionalSleep(5000) {
                        @Override
                        public boolean condition() throws InterruptedException {
                            return getChatbox().contains(Chatbox.MessageType.ALL, "You put the grain in the hopper.")
                                    || getChatbox().contains(Chatbox.MessageType.ALL, "There is already grain in the hopper.");
                        }
                    }.sleep();
                } else {
                    log("Hopper not found or no grain in inventory!");
                }
                List<RS2Object> hopperControls = objects.filter(o -> o.getName().equals("Hopper controls"));
                if (hopperControls.get(0) != null) {
                    sleep(2000);
                    log("Operating Hopper controls...");
                    if (hopperControls.get(0).interact("Operate")) {
                        new ConditionalSleep(5000) {
                            @Override
                            public boolean condition() throws InterruptedException {
                                return getChatbox().contains(Chatbox.MessageType.GAME, "You operate the hopper. The grain slides down the chute.");
                            }
                        }.sleep();
                    }
                }
            } // end of wheat tower actions
        }
        log("Time to collect the wheat!");
        collectWheat();
    }

    private void collectWheat() {
        if (!getInventory().contains("Pot of flour")) {
            walkToArea(wheatTowerBottom, "Bottom of Wheat Tower");
            RS2Object bin = getObjects().closest(wheatTowerBottom, "Flour bin");
            if (bin != null) {
                bin.interact("Empty");
            }
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return getInventory().contains("Pot of flour");
                }
            }.sleep();
        } else {
            log("Not collecting wheat because inventory contains a pot of flour!");
        }
    }

    private void collectFlour() throws InterruptedException {
        if (!getInventory().contains("Pot of flour")) {
            takeItem("Pot", cooksKitchen);
            pickWheat();
            walkToArea(wheatTower, "Wheat Tower");
            hopperInteraction();
        }
    }

    private void collectEgg() {
        takeItem("Egg", chickenFarm);
    }

    private void collectBucket() {
        if (!getInventory().contains("Bucket of milk")) {
            takeItem("Bucket", bucketLoc);
        }
    }

    private void collectMilk() {
        if (getInventory().contains("Bucket") && !getInventory().contains("Bucket of milk")) {
            walkToArea(dairyFarm, "Dairy Farm");
            if (dairyFarm.contains(myPosition())) {
                List<RS2Object> cow = objects.filter(o -> o.getName().equals("Dairy cow"));
                cow.get(0).interact("Milk");
                new ConditionalSleep(7000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getInventory().contains("Bucket of milk") && !myPlayer().isAnimating();
                    }
                }.sleep();
            }
        }
    }

    private boolean questComplete() throws InterruptedException {
        if (getInventory().contains("Egg") && getInventory().contains("Pot of flour") && getInventory().contains("Bucket of milk")) {
            if (!cooksKitchen.contains(myPosition())) {
                walkToArea(cooksKitchen, "Kitchen");
            }
            log("Talking to cook");
            talkToCook();
            return true;
        } else {
            return false;
        }
    }

    private void talkToCook() throws InterruptedException {
        Entity cook = npcs.closest("Cook");
        if (cook != null) {
            if (!getDialogues().inDialogue()) {
                log("Not talking to cook. Starting conversation.");
                cook.interact("Talk-to");
                log("Sleeping until conversation starts!");
                new ConditionalSleep(5000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getDialogues().inDialogue();
                    }
                }.sleep();
            }
            String[] options = new String[]{"Click here to continue",
                    "What's wrong?",
                    "I'm always happy to help a cook in distress.",
                    "Actually, I know where to find this stuff."};
            if (getDialogues().completeDialogue(options)) {
                log("Dialogue complete successfully!");
            } else {
                log("Dialogue failed!");
            }
            log("Just spoke with the cook!");
        }
    }
}