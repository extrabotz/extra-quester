package Interface;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

public class GUI {
    private final JDialog mainDialog;
    private final JComboBox<Quest> questSelector;
    private final DefaultComboBoxModel model = new DefaultComboBoxModel();
    private final JList<Quest> questJList;
    private final ArrayList<Quest> questList = new ArrayList<>();
    private final JScrollPane jScrollPane;

    private boolean started;

    public GUI() {
        mainDialog = new JDialog();
        mainDialog.setTitle("Extra Quester");
        mainDialog.setModalityType(Dialog.ModalityType.MODELESS);
        mainDialog.setResizable(false);
        mainDialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        mainDialog.setLocationByPlatform(true);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        mainPanel.setBorder(new EmptyBorder(20,20,20,20));
        mainDialog.getContentPane().add(mainPanel);

        JPanel questSelectionPanel = new JPanel();
        questSelectionPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel questSelectionLabel = new JLabel("Select a Quest:");
        questSelectionPanel.add(questSelectionLabel);

        questSelector = new JComboBox<>(Quest.values());
        questSelectionPanel.add(questSelector);

        JButton addButton = new JButton("Add Quest");
        addButton.addActionListener( e -> {
            addQuest();
        });
        questSelectionPanel.add(addButton);

        JPanel selectedQuestsPanel = new JPanel();
        selectedQuestsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        questJList = new JList<>(model);
        questJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        questJList.setVisibleRowCount(5);
        jScrollPane = new JScrollPane(questJList);
        selectedQuestsPanel.add(jScrollPane);

        mainPanel.add(questSelectionPanel, BorderLayout.CENTER);
        mainPanel.add(selectedQuestsPanel, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

        JButton removeButton = new JButton("Remove Quest");
        removeButton.addActionListener(e -> {
            removeQuest();
        });
        bottomPanel.add(removeButton);

        JButton startButton = new JButton("Start Questing");
        startButton.addActionListener(e -> {
            startQuesting();
        });
        bottomPanel.add(startButton);
        mainPanel.add(bottomPanel, BorderLayout.PAGE_END);

        mainDialog.pack();
    }

    private void removeQuest() {
        model.removeElement(questJList.getSelectedValue());
    }

    private void addQuest() {
        Object quest = questSelector.getSelectedItem();
        int count = 0;
        for(int i = 0; i < model.getSize(); i++) {
            if(model.getElementAt(i).equals(quest)) {
                count++;
            }
        }
        if(count == 0) {
            model.addElement(quest);
        }
    }

    private void startQuesting() {
        started = true;
        close();
    }

    public boolean isStarted() {
        return started;
    }

    public ArrayList<Quest> getQuestList() {
        for(int i = 0; i < model.getSize(); i++) {
            questList.add((Quest)model.getElementAt(i));
        }
        return questList;
    }

    public void open() {
        mainDialog.setVisible(true);
    }

    public void close() {
        mainDialog.setVisible(false);
        mainDialog.dispose();
    }
}
