package Quests;

import org.osbot.rs07.api.filter.Filter;
import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.api.model.NPC;
import org.osbot.rs07.api.model.RS2Object;
import org.osbot.rs07.api.ui.Tab;
import org.osbot.rs07.utility.ConditionalSleep;

import java.util.List;

public class WitchsHouse extends QuestObject {

    private Area witchLoc;
    private Area ratLoc;
    private Area onionLoc;
    private Area mageLoc;
    private Area meatLoc;
    private Area stoveLoc;
    private Area bank;

    Area[] area = {
            new Area(2966, 3204, 2969, 3207), // witch's location
            new Area(2959, 3202, 2954, 3204), // rat tail location
            new Area(2954, 3249, 2952, 3252), // onion location
            new Area(3012, 3260, 3015, 3257), // mage location
            new Area(3004, 3190, 2990, 3199), // meat location
            new Area(2968, 3210, 2969, 3209), // stove location
            new Area(3010, 3357, 3017, 3355) // falador bank
    };

    private String[] items = new String[]{"Raw meat", "Raw rat meat", "Raw bear meat", "Burnt meat", "Eye of newt", "Onion", "Rats tail"};

    public WitchsHouse() {
        witchLoc = area[0];
        ratLoc = area[1];
        onionLoc = area[2];
        mageLoc = area[3];
        meatLoc = area[4];
        stoveLoc = area[5];
        bank = area[6];
    }

    @Override
    public int run() throws InterruptedException {
        if (!checkInventorySpace()) {
            return 1;
        }
        switch (getConfigs().get(67)) {
            case 0:
                startQuest();
                break;
            case 1:
                if (!getInventory().contains("Rat\'s Tail")) {
                    killAndCollect(ratLoc, "Rat Location", "Rat\'s tail", "Rat");
                    // use the original OSBot loop to handle the fighting
                    return 1;
                } else {
                    log("Inventory already contains: Rat\'s Tail");
                }
                pickOnion();
                collectEyeOfNewt();
                if (!getInventory().contains("Raw meat") && !getInventory().contains("Raw rat meat") && !getInventory().contains("Raw bear meat") && !getInventory().contains("Cooked meat") && !getInventory().contains("Burnt meat")) {
                    killAndCollect(meatLoc, "Big Rat Location", "Raw rat meat", "Giant rat");
                    // use the original OSBot loop to handle the fighting
                    return 1;
                } else {
                    log("Inventory already contains: Rat Meat");
                }
                cookRatMeat();
                finishQuest();
                break;
            case 2:
                drinkFromCauldron();
                break;
            case 3:
                log("Quest is already complete!");
                return 2;
            default:
                log("Witch \'s House: An error occured! Unexpected quest progress occured.");
                return 2;
        }
        return 1;
    }

    /**
     * Check if the inventory is full or doesn't have enough space to collect items
     *
     * @return true if the inventory has enough space to complete the quest
     * @throws InterruptedException
     */
    private boolean checkInventorySpace() throws InterruptedException {
        boolean inventoryFull = getInventory().isFull() || getInventory().getEmptySlots() < 4;
        if (inventoryFull && !bank.contains(myPosition())) {
            walkToArea(bank, "Lumbridge Bank");
            return false;
        } else if (inventoryFull && bank.contains(myPosition())) {
            openBank();
            bankItems();
            takeBankItems();
            return true;
        } else {
            log("Inventory does not need to be banked!");
            return true;
        }
    }

    /**
     * If the bank is not open, then open the bank
     *
     * @throws InterruptedException
     */
    private void openBank() throws InterruptedException {
        if (!getBank().isOpen()) {
            getBank().open();
            log("Opening Bank...");
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return getBank().isOpen();
                }
            }.sleep();
        } else {
            log("Bank is already open!");
        }
    }

    /**
     * Check if there are items related to the quest that are already in the bank
     */
    private void takeBankItems() {
        for (String item : items) {
            if (getBank().contains(item)) {
                log("Found item for quest: " + item);
                getBank().withdraw(item, 1);
            }
        }
    }

    /**
     * Bank all items
     */
    private void bankItems() {
        if (getBank().depositAll()) {
            log("Deposited all items!");
        } else {
            log("There was an issue depositing all items!");
        }
    }

    /**
     * Start the Witch's House quest
     *
     * @throws InterruptedException
     */
    private void startQuest() throws InterruptedException {
        walkToArea(witchLoc, "Witch\'s House");
        talkToWitch();
    }

    /**
     * Finish the Witch's House quest
     *
     * @throws InterruptedException
     */
    private void finishQuest() throws InterruptedException {
        if (getInventory().contains("Onion") && getInventory().contains("Burnt meat") && getInventory().contains("Eye of newt") && getInventory().contains("Burnt meat")) {
            if (!witchLoc.contains(myPosition())) {
                walkToArea(witchLoc, "Witch\'s House");
            }
            log("Talking to witch");
            talkToWitch();
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return !getDialogues().inDialogue();
                }
            }.sleep();
        }
    }

    /**
     * Walk to area
     *
     * @param area     the area object of where to go
     * @param location the name of the location for debugging purposes
     */
    private void walkToArea(Area area, String location) {
        log("Walking to: " + location);
        if (!area.contains(myPosition())) {
            getWalking().webWalk(area);
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return area.contains(myPosition());
                }
            }.sleep();
        } else {
            log("Already in Area: " + location);
        }
    }

    /**
     * Talk to the witch
     *
     * @throws InterruptedException
     */
    private void talkToWitch() throws InterruptedException {
        Entity witch = npcs.closest("Hetty");
        if (witch != null) {
            if (!getDialogues().inDialogue()) {
                log("Not talking to witch. Starting conversation.");
                witch.interact("Talk-to");
                log("Sleeping until conversation starts!");
                new ConditionalSleep(5000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getDialogues().inDialogue();
                    }
                }.sleep();
            }
            String[] options = new String[]{"Click here to continue",
                    "I am in search of a quest.",
                    "Yes, help me become one with my darker side."};
            if (getDialogues().completeDialogue(options)) {
                log("Dialogue completed successfully!");
            } else {
                log("Dialogue failed!");
            }
            log("Just spoke with the witch!");
        }
    }

    /**
     * Go to the magic store and buy the eye of newt
     */
    private void collectEyeOfNewt() {
        if (!getInventory().contains("Eye of newt")) {
            if (getInventory().contains("Coins")) {
                if (getInventory().getItem("Coins").getAmount() >= 3) {
                    walkToArea(mageLoc, "Betty\'s Magic Emporium");
                    Entity betty = npcs.closest("Betty");
                    betty.interact("Trade");
                    new ConditionalSleep(5000) {
                        @Override
                        public boolean condition() throws InterruptedException {
                            return getStore().isOpen();
                        }
                    }.sleep();
                    if (getStore().isOpen()) {
                        getStore().buy("Eye of newt", 1);
                    } else {
                        log("Store isn't open!");
                    }
                } else {
                    log("Not enough coins!");
                }
            }
        } else {
            log("Inventory contains a eye of newt!");
        }
    }

    /**
     * Kill and collect and item in a certain area
     *
     * @param area     the area where to find the NPC
     * @param location the name of the location for debugging purposes
     * @param itemName the string name of the item being collected for debugging purposes
     * @param mobName  the exact name of the NPC to look for
     */
    private void killAndCollect(Area area, String location, String itemName, String mobName) {
        NPC target;
        GroundItem item;
        if (myPlayer().getHealthPercent() <= 20) {
            log("Not enough health!");
            walkToArea(witchLoc, "Witch\'s House");
            log("Sleeping until healed!");
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return myPlayer().getHealthPercent() >= 70;
                }
            }.sleep();
            log("Sleep over!");
            return;
        }
        // if the inventory doesn't contain the item
        if (!getInventory().contains(itemName)) {
            // if my player is not under attack
            if (!myPlayer().isUnderAttack()) {
                // if not already in the area then walk there first
                if (!area.contains(myPosition())) {
                    walkToArea(area, location);
                }
                // target the closest desirable mob
                target = getNpcs().closest(new Filter<NPC>() {
                    @Override
                    public boolean match(NPC npc) {
                        return npc != null && npc.getName().equals(mobName) && !npc.isUnderAttack() && npc.getHealthPercent() > 0;
                    }
                });
                // if the desirable mob is not null
                if (target != null) {
                    // if the target is still alive
                    if (!myPlayer().isInteracting(target)) {
                        target.interact("Attack");
                        new ConditionalSleep(5000) {
                            @Override
                            public boolean condition() throws InterruptedException {
                                return getCombat().isFighting() && myPlayer().isUnderAttack();
                            }
                        }.sleep();
                    }
                }
            } else {
                log("Player is under attack! Continuing OSBot loop...");
            }
        } else {
            log("Inventory already contains: " + itemName);
        }
        item = groundItems.closest(itemName);
        if (item != null) {
            item.interact("Take");
        }
    }

    /**
     * Cook any type of meat in the inventory
     */
    private void cookRatMeat() {
        if (!getInventory().contains("Burnt meat")) {
            walkToArea(stoveLoc, "Kitchen Area");
            if (getInventory().contains("Cooked meat")) {
                getTabs().open(Tab.INVENTORY);
                getInventory().interact("Use", "Cooked meat");
            } else if (getInventory().contains("Raw rat meat")) {
                getTabs().open(Tab.INVENTORY);
                getInventory().interact("Use", "Raw rat meat");
            } else if (getInventory().contains("Raw bear meat")) {
                getTabs().open(Tab.INVENTORY);
                getInventory().interact("Use", "Raw bear meat");
            } else if (getInventory().contains("Raw meat")) {
                getTabs().open(Tab.INVENTORY);
                getInventory().interact("Use", "Raw meat");
            }
            List<RS2Object> stove = objects.filter(o -> o.getName().equals("Range"));
            if (stove.get(0) != null && stove.get(0).interact("Cook") || stove.get(0).interact("Use")) {
                log("Stove used succesfully!");
            }
        } else {
            log("Inventory contains: Burnt Meat");
        }
    }

    /**
     * Walk to the onion farm and pick an onion
     */
    private void pickOnion() {
        if (!getInventory().contains("Onion")) {
            // if the inventory doesn't contain the item by name
            getTabs().open(Tab.INVENTORY);
            walkToArea(onionLoc, "Onion Farm");
            log("In the Onion Area");
            // find the item on the ground
            if (onionLoc.contains(myPosition())) {
                RS2Object item = getObjects().closest(onionLoc, "Onion");
                log("Onion Object: " + item);
                // take the item
                if (item != null) {
                    if (item.interact("Pick")) {
                        log("Onion Picked!");
                        // sleep until the inventory contains a the item picked
                        new ConditionalSleep(5000) {
                            @Override
                            public boolean condition() throws InterruptedException {
                                return getInventory().contains("Onion");
                            }
                        }.sleep();
                        log("Onion picked!");
                    }
                } else {
                    log("Onion not found!");
                }
            }
        }
    }

    /**
     * Drink from the witches cauldron as an RS2Object
     *
     * @throws InterruptedException
     */
    private void drinkFromCauldron() throws InterruptedException {
        List<RS2Object> cauldron = objects.filter(o -> o.getName().equals("Cauldron"));
        if (cauldron != null) {
            cauldron.get(0).interact("Drink From");
        }
        new ConditionalSleep(5000) {
            @Override
            public boolean condition() throws InterruptedException {
                return getDialogues().inDialogue();
            }
        }.sleep();
        String[] options = new String[]{"Click here to continue"};
        if (getDialogues().completeDialogue(options)) {
            log("Dialogue complete successfully!");
        } else {
            log("Dialogue failed!");
        }
        log("Took sip from the cauldron!");
    }

}
