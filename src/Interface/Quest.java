package Interface;

public enum Quest {
    COOKSASSISTANT,
    WITCHSHOUSE,
    XMARKSTHESPOT;

    @Override
    public String toString() {
        switch (name().toLowerCase()) {
            case "cooksassistant":
                return "Cooks Assistant";
            case "witchshouse":
                return "Witch\'s House";
            case "xmarksthespot":
                return "X Marks the Spot";
            default:
                return "Error: Missing Name!";
        }
    }
}