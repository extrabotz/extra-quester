package Quests;

import org.osbot.rs07.script.MethodProvider;

public abstract class QuestObject extends MethodProvider {

    public abstract int run() throws InterruptedException;

}
