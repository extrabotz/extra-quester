package Quests;

import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.utility.ConditionalSleep;

public class XMarksTheSpot extends QuestObject{

    private Area startLoc;
    private Area digArea1;
    private Area digArea2;
    private Area digArea3;
    private Area digArea4;
    private Area finishLoc;
    private Area spadeLoc;
    private Area bank;
    private Area lumbridgeStore;
    private Position digSpot1;
    private Position digSpot2;
    private Position digSpot3;
    private Position digSpot4;

    Area[] area = {
            new Area(3227, 3241, 3229, 3239), // start location
            new Area(3238, 3215, 3231, 3223), // dig area 1
            new Area(3202, 3218, 3203, 3219), // dig area 2
            new Area(3112, 3257, 3110, 3259), // dig area 3
            new Area(3077, 3259, 3076, 3260), // dig area 4
            new Area(3053, 3246, 3052, 3247), // finish location
            new Area(3216, 3410, 3215, 3411).setPlane(1), // spade location
            new Area(3213, 3244, 3209, 3247), // lumbridge market
            new Area(3210, 3219, 3207, 3217).setPlane(2) // Lumbridge Bank
    };

    private String[] items = new String[]{"Spade", "Coins"};

    public XMarksTheSpot() {
        startLoc = area[0];
        digArea1 = area[1];
        digSpot1 = new Position(3230, 3209, 0);
        digArea2 = area[2];
        digSpot2 = new Position(3203, 3213, 0);
        digArea3 = area[3];
        digSpot3 = new Position(3107, 3264, 0);
        digArea4 = area[4];
        digSpot4 = new Position(3078, 3260, 0);
        finishLoc = area[5];
        spadeLoc = area[6];
        lumbridgeStore = area[7];
        bank = area[8];
    }

    @Override
    public int run() throws InterruptedException {
        if(!checkInventory()) {
            return 1;
        }
        switch(getConfigs().get(2111)) {
            case 0:
                startQuest();
                break;
            case 2:
                collectSpade();
                handleSpot(digArea1, digSpot1, "Dig Area 1");
                break;
            case 3:
                handleSpot(digArea2, digSpot2, "Dig Area 2");
                break;
            case 4:
                handleSpot(digArea3, digSpot3, "Dig Area 3");
                break;
            case 5:
                handleSpot(digArea4, digSpot4, "Dig Area 4");
                break;
            case 6:
                completeQuest();
                log("Quest Completed");
                return 2;
            default:
                log("Default error occured! Stopping bot.");
                return 2;
        }
        return 1;
    }

    private void takeItem(String name, Area area) {
        if (!getInventory().contains(name)) {
            // if the inventory doesn't contain the item by name
            walkToArea(area, name);
            // find the item on the ground
            Entity item = getGroundItems().closest(name);
            // take the item
            if (item == null) {
                // sleep until the item exists
                new ConditionalSleep(10000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return item.isVisible();
                    }
                }.sleep();
            } else if (item.interact("Take")) {
                // sleep until the inventory contains the item or the item exists
                new ConditionalSleep(5000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getInventory().contains(name);
                    }
                }.sleep();
            }
        } else {
            log("Inventory Already Contains: " + name);
        }
    }

    private boolean checkInventory() throws InterruptedException {
        boolean inventoryFull = getInventory().isFull() || getInventory().getEmptySlots() < 2;
        if(inventoryFull && !bank.contains(myPosition())) {
            walkToArea(bank, "Lumbridge Bank");
            return false;
        } else if (inventoryFull && bank.contains(myPosition())) {
            openBank();
            bankItems();
            takeBankItems();
            return true;
        } else {
            log("Inventory does not need to be banked!");
            return true;
        }
    }

    private void openBank() throws InterruptedException {
        if(!getBank().isOpen()) {
            getBank().open();
            log("Opening Bank...");
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return getBank().isOpen();
                }
            }.sleep();
        } else {
            log("Bank is already open!");
        }
    }

    private void takeBankItems() {
        for(String item: items) {
            if(getBank().contains(item)) {
                log("Found item for quest: " + item);
                if(item.equals("Coins")) {
                    getBank().withdraw(item, 10);
                } else {
                    getBank().withdraw(item, 1);
                }
                new ConditionalSleep(5000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getInventory().contains(item);
                    }
                }.sleep();
            }
        }
    }

    private void bankItems() {
        if(getBank().depositAll()) {
            log("Deposited all items!");
        } else {
            log("There was an issue depositing all items!");
        }
    }

    private void walkToArea(Area area, String location) {
        log("Walking to: " + location);
        if (!area.contains(myPosition())) {
            getWalking().webWalk(area);
            new ConditionalSleep(5000) {
                @Override
                public boolean condition() throws InterruptedException {
                    return area.contains(myPosition());
                }
            }.sleep();
        } else {
            log("Already in Area: " + location);
        }
    }

    private void startQuest() throws InterruptedException {
        walkToArea(startLoc, "The Sheared Ram Pub");
        talkToVeos();
    }

    private void talkToVeos() throws InterruptedException {
        Entity veos = npcs.closest("Veos");
        if (veos != null) {
            if (!getDialogues().inDialogue()) {
                log("Not talking to Veos. Starting conversation.");
                veos.interact("Talk-to");
                log("Sleeping until conversation starts!");
                new ConditionalSleep(5000) {
                    @Override
                    public boolean condition() throws InterruptedException {
                        return getDialogues().inDialogue();
                    }
                }.sleep();
            }
            String[] options = new String[]{"Click here to continue",
                    "I'm looking for a quest.",
                    "Sounds good, what should I do?",
                    "Okay, thanks Veos."};
            if (getDialogues().completeDialogue(options)) {
                log("Dialogue complete successfully!");
            } else {
                log("Dialogue failed!");
            }
            log("Just spoke with Veos!");
        }
    }

    private void collectSpade() {
        if(!getInventory().contains("Spade")) {
            if(getInventory().contains("Coins")) {
                if (getInventory().getItem("Coins").getAmount() >= 3) {
                    walkToArea(lumbridgeStore, "Lumbridge Store");
                    Entity shopKeeper = npcs.closest("Shop keeper");
                    shopKeeper.interact("Trade");
                    new ConditionalSleep(5000) {
                        @Override
                        public boolean condition() throws InterruptedException {
                            return getStore().isOpen();
                        }
                    }.sleep();
                    if(getStore().isOpen()) {
                        getStore().buy("Spade", 1);
                    } else {
                        log("Store isn't open!");
                        collectSpadeInVarrock();
                    }
                } else {
                    collectSpadeInVarrock();
                }
            } else {
                collectSpadeInVarrock();
            }
        } else {
            log("Inventory contains a spade!");
        }
    }

    private void collectSpadeInVarrock() {
        log("Not enough coins! Retrieving free shovel from Varrock.");
        walkToArea(spadeLoc, "Spade Location");
        takeItem("Spade", spadeLoc);
    }

    private void completeQuest() throws InterruptedException {
        walkToArea(finishLoc, "Final Location");
        talkToVeos();
    }

    private void handleSpot(Area area,  Position point, String location) {
        int status = getConfigs().get(2111) + 1;
        walkToArea(area,location);
        getWalking().webWalk(point);
        new ConditionalSleep(5000) {
            @Override
            public boolean condition() throws InterruptedException {
                return myPosition().getX() == point.getX() && myPosition().getY() == point.getY();
            }
        }.sleep();
        Item spade = getInventory().getItem("Spade");
        spade.interact("Dig");
        new ConditionalSleep(5000) {
            @Override
            public boolean condition() throws InterruptedException {
                return !myPlayer().isAnimating() && getConfigs().get(2111) == status;
            }
        }.sleep();
    }


}
