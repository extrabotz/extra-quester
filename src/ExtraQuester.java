import Interface.*;
import Quests.*;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

@ScriptManifest(author = "ExtraBotz", name = "Extra Quester", info = "F2P Quests", version = 1.0, logo = "https://i.imgur.com/zkLNldq.png")
public final class ExtraQuester extends Script {

    private GUI gui = new GUI();
    private ArrayList<Quest> quests;
    private Quest quest;
    private QuestObject selectedQuest;
    private int status;

    @Override
    public void onStart() {
        try {
            SwingUtilities.invokeAndWait(() -> {
                gui = new GUI();
                gui.open();
            });
        } catch (InterruptedException | InvocationTargetException e) {
            e.printStackTrace();
            stop();
            return;
        }
        // If the user closed the dialog and didn't click the Start button
        if (!gui.isStarted()) {
            stop();
            return;
        }
        quests = gui.getQuestList();
        nextQuest();
        log("Quest: " + quest);
    }

    @Override
    public int onLoop() throws InterruptedException {
        log("Running: " + status);
        switch(status) {
            case 0:
                log("No more quests to complete! Stopping now...");
                stop(false);
                break;
            case 1:
                log("Selected Quest: " + quest.toString());
                if(selectedQuest != null) {
                    log("Starting Quest...");
                    status = selectedQuest.run();
                } else {
                    log("There was an issue creating a quest object!");
                    status = 0;
                }
                break;
            case 2:
                nextQuest();
                break;
        }
        return 500;
    }

    public void nextQuest() {
        if(quests.size() > 0) {
            quest = quests.get(0);
            quests.remove(quest);
            selectedQuest = questSelection();
            status = 1;
        } else {
            status = 0;
        }
    }

    private QuestObject questSelection() {
        log("Quest Selection...");
        switch(quest.toString()) {
            case "Cooks Assistant":
                selectedQuest = new CooksAssistant();
                status = 1;
                break;
            case "X Marks the Spot":
                selectedQuest = new XMarksTheSpot();
                status = 1;
                break;
            case "Witch\'s House":
                selectedQuest = new WitchsHouse();
                status = 1;
                break;
            default:
                log("Error! Quest not found in quest selection process.");
                status = 0;
                break;
        }
        selectedQuest.exchangeContext(getBot());
        log(quest.toString() + " Selected...");
        return selectedQuest;
    }

    @Override
    public void onExit() {
        if (gui != null) {
            gui.close();
        }
    }

}
